REQUIREMENTS:
jq
oathtool

The purpose of this script is to allow you to generate totp codes where you
know the secret. I use this script instead of a phone because of the large
number of unsearchable totp codes in my phone's authenticator app.

How to use this script:

Copy this script to a directory located in your path.
I like to use ~/bin and add that directory to my path.

mkdir ~/bin

Then add the following line to my ~/.bashrc:

export PATH=${PATH}:~/bin

Create a json file in the location specified by ${MFAKEYFILE} such as ~/.totp-keys.json:

{ "sitename1": "TOTP_SECRET1",
  "sitename2": "TOTP_SECRET2" }

Set permissions with chmod to only allow the owner to access it:
'chmod 600 ~/.totp-keys.json'

This file shall contain a key:value pair for each totp site.
The 'key' is the site name. This can be any name you choose, but must be unique
within the file. (Don't include spaces in the name.)
The 'value' is the 'TOTP SECRET' you normally obtain by scanning a QR Code.

This script can be used to generate a totp code for a site without needing to use a separate authenticator app.

Example:
$ totp sitename
123321

Output will be the TOTP 6-digit code needed as a 2FA for the site.
The code will be based on your system's date/time, so make sure it is set
correctly.

If you have suggestions, please open an Issue on GitLab:
https://gitlab.com/TheLinuxNinja/totp/issues
